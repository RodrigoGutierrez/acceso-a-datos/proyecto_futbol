<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asistencias".
 *
 * @property int $id_asistencia
 * @property int|null $codigo_asistencia
 * @property int|null $codigo_gol
 * @property string $tipo
 *
 * @property Goles $codigoGol
 * @property AsistenciasRealizadas[] $asistenciasRealizadas
 * @property Jugadores[] $codigoJugadors
 * @property Partidos[] $codigoPartidos
 * @property Goles $goles
 */
class Asistencias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asistencias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_asistencia', 'codigo_gol'], 'integer'],
            [['tipo'], 'required'],
            [['tipo'], 'string', 'max' => 20],
            [['codigo_asistencia', 'codigo_gol'], 'unique', 'targetAttribute' => ['codigo_asistencia', 'codigo_gol']],
            [['codigo_gol'], 'exist', 'skipOnError' => true, 'targetClass' => Goles::className(), 'targetAttribute' => ['codigo_gol' => 'codigo_gol']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_asistencia' => 'Id Asistencia',
            'codigo_asistencia' => 'Codigo Asistencia',
            'codigo_gol' => 'Codigo Gol',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * Gets query for [[CodigoGol]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoGol()
    {
        return $this->hasOne(Goles::className(), ['codigo_gol' => 'codigo_gol']);
    }

    /**
     * Gets query for [[AsistenciasRealizadas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistenciasRealizadas()
    {
        return $this->hasMany(AsistenciasRealizadas::className(), ['id_asistencia' => 'id_asistencia']);
    }

    /**
     * Gets query for [[CodigoJugadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadors()
    {
        return $this->hasMany(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador'])->viaTable('asistencias_realizadas', ['id_asistencia' => 'id_asistencia']);
    }

    /**
     * Gets query for [[CodigoPartidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartidos()
    {
        return $this->hasMany(Partidos::className(), ['codigo_partido' => 'codigo_partido'])->viaTable('asistencias_realizadas', ['id_asistencia' => 'id_asistencia']);
    }

    /**
     * Gets query for [[Goles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGoles()
    {
        return $this->hasOne(Goles::className(), ['id_asistencia' => 'id_asistencia']);
    }
}
