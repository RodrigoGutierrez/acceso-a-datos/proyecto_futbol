<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipos".
 *
 * @property int $codigo_equipo
 * @property string $nombre_equipo
 * @property int|null $es_seleccion
 * @property string|null $entrenador
 * @property string|null $estadio
 *
 * @property JugadoresEnEquipos[] $jugadoresEnEquipos
 * @property Jugadores[] $codigoJugadors
 * @property Partidos[] $partidos
 * @property Partidos[] $partidos0
 */
class Equipos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_equipo'], 'required'],
            [['es_seleccion'], 'integer'],
            [['nombre_equipo', 'entrenador'], 'string', 'max' => 25],
            [['estadio'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_equipo' => 'Codigo Equipo',
            'nombre_equipo' => 'Nombre Equipo',
            'es_seleccion' => 'Es Seleccion',
            'entrenador' => 'Entrenador',
            'estadio' => 'Estadio',
        ];
    }

    /**
     * Gets query for [[JugadoresEnEquipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadoresEnEquipos()
    {
        return $this->hasMany(JugadoresEnEquipos::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[CodigoJugadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadors()
    {
        return $this->hasMany(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador'])->viaTable('jugadores_en_equipos', ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasMany(Partidos::className(), ['codigo_equipo_casa' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Partidos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos0()
    {
        return $this->hasMany(Partidos::className(), ['codigo_equipo_fuera' => 'codigo_equipo']);
    }
}
