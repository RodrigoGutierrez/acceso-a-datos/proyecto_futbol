<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $codigo_jugador
 * @property string $nombre
 * @property string $apellidos
 * @property int|null $dorsal
 * @property string|null $fecha_nac
 * @property string|null $representante
 *
 * @property AsistenciasRealizadas[] $asistenciasRealizadas
 * @property Asistencias[] $asistencias
 * @property GolesMarcadaos[] $golesMarcadaos
 * @property Goles[] $codigoGols
 * @property JugadoresEnEquipos[] $jugadoresEnEquipos
 * @property Equipos[] $codigoEquipos
 * @property Patrocinadores[] $patrocinadores
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos'], 'required'],
            [['dorsal'], 'integer'],
            [['fecha_nac'], 'safe'],
            [['nombre', 'representante'], 'string', 'max' => 30],
            [['apellidos'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_jugador' => 'Codigo Jugador',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'dorsal' => 'Dorsal',
            'fecha_nac' => 'Fecha Nac',
            'representante' => 'Representante',
        ];
    }

    /**
     * Gets query for [[AsistenciasRealizadas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistenciasRealizadas()
    {
        return $this->hasMany(AsistenciasRealizadas::className(), ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[Asistencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistencias()
    {
        return $this->hasMany(Asistencias::className(), ['id_asistencia' => 'id_asistencia'])->viaTable('asistencias_realizadas', ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[GolesMarcadaos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGolesMarcadaos()
    {
        return $this->hasMany(GolesMarcadaos::className(), ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[CodigoGols]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoGols()
    {
        return $this->hasMany(Goles::className(), ['codigo_gol' => 'codigo_gol'])->viaTable('goles_marcadaos', ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[JugadoresEnEquipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadoresEnEquipos()
    {
        return $this->hasMany(JugadoresEnEquipos::className(), ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[CodigoEquipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipos()
    {
        return $this->hasMany(Equipos::className(), ['codigo_equipo' => 'codigo_equipo'])->viaTable('jugadores_en_equipos', ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[Patrocinadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatrocinadores()
    {
        return $this->hasMany(Patrocinadores::className(), ['codigo_jugador' => 'codigo_jugador']);
    }
}
