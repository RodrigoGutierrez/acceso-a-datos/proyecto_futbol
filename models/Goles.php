<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goles".
 *
 * @property int $codigo_gol
 * @property int|null $id_asistencia
 * @property string $tipo
 *
 * @property Asistencias[] $asistencias
 * @property Asistencias $asistencia
 * @property GolesMarcadaos[] $golesMarcadaos
 * @property Jugadores[] $codigoJugadors
 * @property Partidos[] $codigoPartidos
 */
class Goles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_asistencia'], 'integer'],
            [['tipo'], 'required'],
            [['tipo'], 'string', 'max' => 20],
            [['id_asistencia'], 'unique'],
            [['id_asistencia'], 'exist', 'skipOnError' => true, 'targetClass' => Asistencias::className(), 'targetAttribute' => ['id_asistencia' => 'id_asistencia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_gol' => 'Codigo Gol',
            'id_asistencia' => 'Id Asistencia',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * Gets query for [[Asistencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistencias()
    {
        return $this->hasMany(Asistencias::className(), ['codigo_gol' => 'codigo_gol']);
    }

    /**
     * Gets query for [[Asistencia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistencia()
    {
        return $this->hasOne(Asistencias::className(), ['id_asistencia' => 'id_asistencia']);
    }

    /**
     * Gets query for [[GolesMarcadaos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGolesMarcadaos()
    {
        return $this->hasMany(GolesMarcadaos::className(), ['codigo_gol' => 'codigo_gol']);
    }

    /**
     * Gets query for [[CodigoJugadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadors()
    {
        return $this->hasMany(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador'])->viaTable('goles_marcadaos', ['codigo_gol' => 'codigo_gol']);
    }

    /**
     * Gets query for [[CodigoPartidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartidos()
    {
        return $this->hasMany(Partidos::className(), ['codigo_partido' => 'codigo_partido'])->viaTable('goles_marcadaos', ['codigo_gol' => 'codigo_gol']);
    }
}
