<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asistencias_realizadas".
 *
 * @property int $id
 * @property int|null $codigo_partido
 * @property int|null $id_asistencia
 * @property int|null $codigo_jugador
 * @property int|null $minuto
 *
 * @property Asistencias $asistencia
 * @property Jugadores $codigoJugador
 * @property Partidos $codigoPartido
 */
class AsistenciasRealizadas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asistencias_realizadas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'codigo_partido', 'id_asistencia', 'codigo_jugador', 'minuto'], 'integer'],
            [['codigo_jugador', 'codigo_partido', 'id_asistencia'], 'unique', 'targetAttribute' => ['codigo_jugador', 'codigo_partido', 'id_asistencia']],
            [['codigo_jugador', 'id_asistencia'], 'unique', 'targetAttribute' => ['codigo_jugador', 'id_asistencia']],
            [['codigo_partido', 'id_asistencia'], 'unique', 'targetAttribute' => ['codigo_partido', 'id_asistencia']],
            [['id'], 'unique'],
            [['id_asistencia'], 'exist', 'skipOnError' => true, 'targetClass' => Asistencias::className(), 'targetAttribute' => ['id_asistencia' => 'id_asistencia']],
            [['codigo_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigo_jugador' => 'codigo_jugador']],
            [['codigo_partido'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::className(), 'targetAttribute' => ['codigo_partido' => 'codigo_partido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_partido' => 'Codigo Partido',
            'id_asistencia' => 'Id Asistencia',
            'codigo_jugador' => 'Codigo Jugador',
            'minuto' => 'Minuto',
        ];
    }

    /**
     * Gets query for [[Asistencia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistencia()
    {
        return $this->hasOne(Asistencias::className(), ['id_asistencia' => 'id_asistencia']);
    }

    /**
     * Gets query for [[CodigoJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugador()
    {
        return $this->hasOne(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[CodigoPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartido()
    {
        return $this->hasOne(Partidos::className(), ['codigo_partido' => 'codigo_partido']);
    }
}
