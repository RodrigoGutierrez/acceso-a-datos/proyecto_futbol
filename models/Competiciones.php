<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "competiciones".
 *
 * @property int $codigo_competicion
 * @property string $nombre
 * @property string|null $lugar
 * @property string|null $ganador
 * @property string|null $tipo
 * @property int|null $num_equipos
 * @property string|null $anio_inicio
 * @property string|null $anio_fin
 *
 * @property Partidos[] $partidos
 */
class Competiciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'competiciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['num_equipos'], 'integer'],
            [['anio_inicio', 'anio_fin'], 'safe'],
            [['nombre', 'lugar', 'tipo'], 'string', 'max' => 20],
            [['ganador'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_competicion' => 'Codigo Competicion',
            'nombre' => 'Nombre',
            'lugar' => 'Lugar',
            'ganador' => 'Ganador',
            'tipo' => 'Tipo',
            'num_equipos' => 'Num Equipos',
            'anio_inicio' => 'Anio Inicio',
            'anio_fin' => 'Anio Fin',
        ];
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasMany(Partidos::className(), ['codigo_competicion' => 'codigo_competicion']);
    }
}
