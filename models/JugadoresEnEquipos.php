<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores_en_equipos".
 *
 * @property int $id
 * @property int|null $codigo_jugador
 * @property int|null $codigo_equipo
 * @property string|null $fecha_inicio
 * @property string|null $fecha_fin
 *
 * @property Equipos $codigoEquipo
 * @property Jugadores $codigoJugador
 */
class JugadoresEnEquipos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores_en_equipos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_jugador', 'codigo_equipo'], 'integer'],
            [['fecha_inicio', 'fecha_fin'], 'safe'],
            [['codigo_jugador', 'codigo_equipo'], 'unique', 'targetAttribute' => ['codigo_jugador', 'codigo_equipo']],
            [['codigo_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo' => 'codigo_equipo']],
            [['codigo_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigo_jugador' => 'codigo_jugador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_jugador' => 'Codigo Jugador',
            'codigo_equipo' => 'Codigo Equipo',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
        ];
    }

    /**
     * Gets query for [[CodigoEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipo()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[CodigoJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugador()
    {
        return $this->hasOne(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador']);
    }
}
