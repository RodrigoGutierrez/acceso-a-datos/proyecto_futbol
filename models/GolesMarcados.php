<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goles_marcados".
 *
 * @property int $id
 * @property int|null $codigo_partido
 * @property int|null $codigo_gol
 * @property int|null $codigo_jugador
 * @property int|null $minuto
 *
 * @property Goles $codigoGol
 * @property Jugadores $codigoJugador
 * @property Partidos $codigoPartido
 */
class GolesMarcados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goles_marcados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_partido', 'codigo_gol', 'codigo_jugador', 'minuto'], 'integer'],
            [['codigo_jugador', 'codigo_partido', 'codigo_gol'], 'unique', 'targetAttribute' => ['codigo_jugador', 'codigo_partido', 'codigo_gol']],
            [['codigo_jugador', 'codigo_gol'], 'unique', 'targetAttribute' => ['codigo_jugador', 'codigo_gol']],
            [['codigo_partido', 'codigo_gol'], 'unique', 'targetAttribute' => ['codigo_partido', 'codigo_gol']],
            [['codigo_gol'], 'exist', 'skipOnError' => true, 'targetClass' => Goles::className(), 'targetAttribute' => ['codigo_gol' => 'codigo_gol']],
            [['codigo_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigo_jugador' => 'codigo_jugador']],
            [['codigo_partido'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::className(), 'targetAttribute' => ['codigo_partido' => 'codigo_partido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_partido' => 'Codigo Partido',
            'codigo_gol' => 'Codigo Gol',
            'codigo_jugador' => 'Codigo Jugador',
            'minuto' => 'Minuto',
        ];
    }

    /**
     * Gets query for [[CodigoGol]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoGol()
    {
        return $this->hasOne(Goles::className(), ['codigo_gol' => 'codigo_gol']);
    }

    /**
     * Gets query for [[CodigoJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugador()
    {
        return $this->hasOne(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[CodigoPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartido()
    {
        return $this->hasOne(Partidos::className(), ['codigo_partido' => 'codigo_partido']);
    }
}
