<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patrocinadores".
 *
 * @property int $codigo_patrocinador
 * @property int|null $codigo_jugador
 * @property string|null $nombre_patrocinador
 *
 * @property Jugadores $codigoJugador
 */
class Patrocinadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patrocinadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_patrocinador'], 'required'],
            [['codigo_patrocinador', 'codigo_jugador'], 'integer'],
            [['nombre_patrocinador'], 'string', 'max' => 30],
            [['codigo_jugador', 'nombre_patrocinador'], 'unique', 'targetAttribute' => ['codigo_jugador', 'nombre_patrocinador']],
            [['codigo_patrocinador'], 'unique'],
            [['codigo_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigo_jugador' => 'codigo_jugador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_patrocinador' => 'Codigo Patrocinador',
            'codigo_jugador' => 'Codigo Jugador',
            'nombre_patrocinador' => 'Nombre Patrocinador',
        ];
    }

    /**
     * Gets query for [[CodigoJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugador()
    {
        return $this->hasOne(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador']);
    }
}
