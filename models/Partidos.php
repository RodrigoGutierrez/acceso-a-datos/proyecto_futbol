<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidos".
 *
 * @property int $codigo_partido
 * @property string|null $fecha
 * @property string|null $hora
 * @property string|null $resultado
 * @property string|null $estadio
 * @property int|null $codigo_competicion
 * @property int|null $codigo_equipo_fuera
 * @property int|null $codigo_equipo_casa
 *
 * @property AsistenciasRealizadas[] $asistenciasRealizadas
 * @property Asistencias[] $asistencias
 * @property GolesMarcadaos[] $golesMarcadaos
 * @property Goles[] $codigoGols
 * @property Competiciones $codigoCompeticion
 * @property Equipos $codigoEquipoCasa
 * @property Equipos $codigoEquipoFuera
 */
class Partidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora'], 'safe'],
            [['codigo_competicion', 'codigo_equipo_fuera', 'codigo_equipo_casa'], 'integer'],
            [['resultado'], 'string', 'max' => 6],
            [['estadio'], 'string', 'max' => 35],
            [['codigo_competicion'], 'exist', 'skipOnError' => true, 'targetClass' => Competiciones::className(), 'targetAttribute' => ['codigo_competicion' => 'codigo_competicion']],
            [['codigo_equipo_casa'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo_casa' => 'codigo_equipo']],
            [['codigo_equipo_fuera'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo_fuera' => 'codigo_equipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_partido' => 'Codigo Partido',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'resultado' => 'Resultado',
            'estadio' => 'Estadio',
            'codigo_competicion' => 'Codigo Competicion',
            'codigo_equipo_fuera' => 'Codigo Equipo Fuera',
            'codigo_equipo_casa' => 'Codigo Equipo Casa',
        ];
    }

    /**
     * Gets query for [[AsistenciasRealizadas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistenciasRealizadas()
    {
        return $this->hasMany(AsistenciasRealizadas::className(), ['codigo_partido' => 'codigo_partido']);
    }

    /**
     * Gets query for [[Asistencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistencias()
    {
        return $this->hasMany(Asistencias::className(), ['id_asistencia' => 'id_asistencia'])->viaTable('asistencias_realizadas', ['codigo_partido' => 'codigo_partido']);
    }

    /**
     * Gets query for [[GolesMarcadaos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGolesMarcadaos()
    {
        return $this->hasMany(GolesMarcadaos::className(), ['codigo_partido' => 'codigo_partido']);
    }

    /**
     * Gets query for [[CodigoGols]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoGols()
    {
        return $this->hasMany(Goles::className(), ['codigo_gol' => 'codigo_gol'])->viaTable('goles_marcadaos', ['codigo_partido' => 'codigo_partido']);
    }

    /**
     * Gets query for [[CodigoCompeticion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCompeticion()
    {
        return $this->hasOne(Competiciones::className(), ['codigo_competicion' => 'codigo_competicion']);
    }

    /**
     * Gets query for [[CodigoEquipoCasa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipoCasa()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo_casa']);
    }

    /**
     * Gets query for [[CodigoEquipoFuera]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipoFuera()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo_fuera']);
    }
}
