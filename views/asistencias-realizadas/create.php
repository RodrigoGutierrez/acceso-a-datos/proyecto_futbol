<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AsistenciasRealizadas */

$this->title = 'Create Asistencias Realizadas';
$this->params['breadcrumbs'][] = ['label' => 'Asistencias Realizadas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asistencias-realizadas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
