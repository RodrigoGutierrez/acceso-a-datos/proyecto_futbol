<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asistencias Realizadas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asistencias-realizadas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Asistencias Realizadas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'codigo_partido',
            'id_asistencia',
            'codigo_jugador',
            'minuto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
