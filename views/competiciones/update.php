<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Competiciones */

$this->title = 'Update Competiciones: ' . $model->codigo_competicion;
$this->params['breadcrumbs'][] = ['label' => 'Competiciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_competicion, 'url' => ['view', 'id' => $model->codigo_competicion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="competiciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
