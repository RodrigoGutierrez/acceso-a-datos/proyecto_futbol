<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Competiciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="competiciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Competiciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_competicion',
            'nombre',
            'lugar',
            'ganador',
            'tipo',
            //'num_equipos',
            //'anio_inicio',
            //'anio_fin',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
