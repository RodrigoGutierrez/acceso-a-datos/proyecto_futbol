<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JugadoresEnEquipos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jugadores-en-equipos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_jugador')->textInput() ?>

    <?= $form->field($model, 'codigo_equipo')->textInput() ?>

    <?= $form->field($model, 'fecha_inicio')->textInput() ?>

    <?= $form->field($model, 'fecha_fin')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
