<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GolesMarcados */

$this->title = 'Update Goles Marcados: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Goles Marcados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="goles-marcados-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
