<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Goles Marcados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goles-marcados-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Goles Marcados', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'codigo_partido',
            'codigo_gol',
            'codigo_jugador',
            'minuto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
