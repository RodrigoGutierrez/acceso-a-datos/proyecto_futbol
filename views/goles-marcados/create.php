<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GolesMarcados */

$this->title = 'Create Goles Marcados';
$this->params['breadcrumbs'][] = ['label' => 'Goles Marcados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goles-marcados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
