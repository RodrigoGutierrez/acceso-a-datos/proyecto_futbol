<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GolesMarcados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="goles-marcados-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_partido')->textInput() ?>

    <?= $form->field($model, 'codigo_gol')->textInput() ?>

    <?= $form->field($model, 'codigo_jugador')->textInput() ?>

    <?= $form->field($model, 'minuto')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
