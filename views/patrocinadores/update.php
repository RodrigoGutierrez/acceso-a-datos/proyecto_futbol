<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Patrocinadores */

$this->title = 'Update Patrocinadores: ' . $model->codigo_patrocinador;
$this->params['breadcrumbs'][] = ['label' => 'Patrocinadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_patrocinador, 'url' => ['view', 'id' => $model->codigo_patrocinador]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="patrocinadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
