<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Partidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partidos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Partidos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_partido',
            'fecha',
            'hora',
            'resultado',
            'estadio',
            //'codigo_competicion',
            //'codigo_equipo_fuera',
            //'codigo_equipo_casa',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
